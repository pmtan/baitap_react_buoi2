import React, { Component } from "react";
import styles from "./thuKinh.module.css";
import { data } from "./dataGlasses";

export default class ThuKinh extends Component {
  state = {
    glasses: data,
    currentGlasses: data[0],
  };
  renderGlassesList = () => {
    return this.state.glasses.map((item) => {
      return (
        <img
          onClick={() => {
            this.handleChangeGlasses(item);
          }}
          key={item.id}
          src={item.url}
          className={styles.glasses}
          alt=""
        />
      );
    });
  };

  renderModel = () => {
    return (
      <div className={styles.model}>
        <img
          key={"model"}
          className={styles.modelImg}
          src="/glassesImage/model.jpg"
          alt=""
        />
        <img
          className={styles.glassesOn}
          src={this.state.currentGlasses.url}
          alt=""
        />
        <div className={styles.glassesInfo}>
          <h5 className="text-primary">{this.state.currentGlasses.name}</h5>
          <p className="text-white">{this.state.currentGlasses.desc}</p>
        </div>
      </div>
    );
  };

  handleChangeGlasses = (data) => {
    this.setState({
      currentGlasses: data,
    });
  };
  render() {
    return (
      <div>
        <div className={styles.background}>
          <header>
            <h1 className="bg-secondary p-3">TRY GLASSES APP ONLINE</h1>
          </header>
          <div className="container mt-5">
            {this.renderModel()}
            <div className="bg-light m-5">{this.renderGlassesList()}</div>
          </div>
        </div>
      </div>
    );
  }
}
